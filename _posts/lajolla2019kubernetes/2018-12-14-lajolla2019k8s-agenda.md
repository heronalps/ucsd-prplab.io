---
layout: workshop
title: "La Jolla Kubernetes-Grafana workshop agenda"
date: 2019-02-19
workshop: lajolla2019k8s
short: lajolla2019k8s
img: lajolla2019k8s/k8s.png
---

All materials presented during the workshop are linked on this page (see power point slides and Exercises).
<br>If you are interested in doing hands-on exercises during the work shop please see [Materials](/lajolla2019k8s-materials)
<br><b>Note: </b>Catering will be provided on-site for Breakfast, Lunch and Breaks on the 5th floor

<table class="workshop">
<tr>
    <th>Time</th>
    <th>Activity</th>
</tr>
<tr>
  <td class="break">08:00 - 09:00</td>
  <td class="break">Breakfast</td>
</tr>
<tr>
  <td>09:00 - 10:30</td>
  <td>Basic Kubernetes setup and functionality on the PRP 
  <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/kubernetes-grafana/slides/kubernetes-intro.pptx">(power point slides)</a>
  <br><a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/tree/master/kubernetes-grafana">Exercise materials</a>
  </td>
</tr>
<tr>
  <td class="break">10:30 - 10:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>10:45 - 12:00</td>
  <td>Grafana for monitoring and measuring usage 
  <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/kubernetes-grafana/slides/grafana-intro.pptx">(power point slides)</a>
  </td>
</tr>
<tr>
  <td class="break">12:00 - 13:00</td>
  <td class="break">Lunch</td>
</tr>
<tr>
  <td>13:00 - 14:00</td>
  <td>Policy writing for access, scheduling, monitoring, and measuring 
  <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/kubernetes-grafana/slides/kubernetes-policy.pptx">(power point slides)</a>
  </td>
</tr>
<tr>
  <td>14:00 - 14:30</td>
  <td>Federating Kubernetes clusters (cancelled)</td>
</tr>
<tr>
  <td class="break">14:30 - 14:45</td>
  <td class="break">Break</td>
</tr>
<tr>
  <td>14:45 - 16:00</td>
  <td>Security in Kubernetes, CIlogon, etc. 
  <a href="https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master/kubernetes-grafana/slides/kubernetes-security.pptx">(power point slides)</a>
  </td>
</tr>
</table>

[Back to top]({{page.url}})


---
layout: workshop
title: "Workshop Materials"
date: 2018-12-14
workshop: lajolla2019
short: lajolla2019
img: lajolla2019/header.png
---

<div class="font-weight-bold text-info" >Useful Links</div>
- [https://www.perfsonar.net/][2] 
- [https://learn.nsrc.org/perfsonar][3] 
- [https://fasterdata.es.net/science-dmz/][4] 
- [https://learn.nsrc.org/science-dmz][5]

Links to workshop presentations, labs and references will be placed in [Gitlab Repository][1]

{% assign base='https://gitlab.com/ucsd-prp/presentations/lajolla-2019/blob/master' %}

[1]: {{base}}/README.md
[2]: https://www.perfsonar.net/
[3]: https://learn.nsrc.org/perfsonar
[4]: https://fasterdata.es.net/science-dmz/
[5]: https://learn.nsrc.org/science-dmz

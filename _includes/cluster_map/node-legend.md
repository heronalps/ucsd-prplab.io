<div class="legend-container">
  <h5 class="dfn">Marker legend</h5>
  <b class="dfn">size: </b>amount of memory
  <table class="legend-table">
    <tr>
      <th>Marker Size</th>
      <th>Memory Range</th>
    </tr>
    {% for marker_size in site.data.cluster_map.node_mem_legend %}
      <tr>
        <td>{{ marker_size[0] }}</td>
        <td>{{ marker_size[1] }}</td>
      </tr>
    {% endfor %}
  </table>
  <b class="dfn">label: </b>number of GPUs<br>
  <b class="dfn">color: </b>number of CPUs<br>
  <img src="/images/cluster_map/node_cpu_legend.png" width="80%"/>
</div>

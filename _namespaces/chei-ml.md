---
layout: namespace
title: Chei-ml
date: 2019-01-09
name: chei-ml
pi: Falko Kuester and Stuart Sandin
institution: University of California, San Diego and SIO
software: Caffe, PyTorch, Conda, Kubernetes, Pandas, OpenCV
tagline: Deep learning for coral species segmentation
imagesrc: chei-ml.png
categories: 
- "namespace" 
tags: []
---

Using structure from motion, we are able to reconstruct 3D point cloud models of coral reefs 
from photographic surveys. We are leveraging advances in 2D and 3D computer vision to increase 
the speed and accuracy of the annotation of these models for use in ecological and biological research.

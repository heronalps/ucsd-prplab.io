---
layout: namespace
title: UCSD-haosulab
date: 2019-01-14
name: ucsd-haosulab
pi: Hao Su
institution: University of California, San Diego
software: Pytorch, Conda
tagline: Machine learning methods based on point cloud
imagesrc: ucsd-haosu.png
categories: 
- "namespace" 
tags: [3D modeling]
---

Hao Su's lab is working on refinement and acceleration of machine learning methods based on point cloud, 
like PointNet and its variants. The improved efficiency and accuracy will help many real-world 
applications including self-driving car.

---
layout: namespace
title: Ecewcsng
date: 2018-12-11
name: ecewcsng
pi: Dinesh Bharadia
institution: University of California, San Diego 
software: conda, tensorflow, caffe, numpy, opencv
tagline: Deep Learning for sensor data processing 
imagesrc: ecewcsng.png
categories: 
- "namespace" 
tags: []
---

We are working on Collaborative or Cooperative Autonomous driving and sensing
focusing on efficient processing (in terms of usage of data and compute) of image 
and videos for assisted and autonomous driving applications.
The project is attempting to build deep learning algorithms which can effectively combine 
data from other autonomous systems for safety applications. We are trying to combine data 
from all the sensors on other cars for self-driving cars for safety applications. 
The sensors include LiDAR, Camera and radars, CAN data and so on.

---
layout: collection-userdocs
title: GitLab
date: 2019-02-08
short: userdocs
categories: user
order: 1
---

We use our own installation of [GitLab][1] for [Source Code Management][2], [Continuous Integration automation][3], 
docker containers registry and other development lifecycle tasks. It fully uses Nautilus Cluster resources, which provides our users unlimited storage and fast builds.
All data from our GitLab except container images are backed up nightly to Google S3. 

##### Step 1: Create a Git repo
1. To use our GItLab installation, register at [https://gitlab.nautilus.optiputer.net][4]
1. Use GitLab for storing your code like any git repository. Here's [GitLab basics guide][5].
1. [create a new project][project] in your GitLab account 

##### Step 2: Use Containers Registry
What makes GitLab especially useful for kubernetes cluster in integration with
Docker Registry. You can store your containers directly in our cluster and
avoid slow downloads from [DockerHub][dockerhub] (although you're still free to do that as well). To use containers registry: 
1. go to your project's general settings and enable the Container Registry. See this instructions [Container Registry][registry]. 
  (It's enabled by default for all projects in our GitLab)

##### Step 3: Continuous Integration automation
To fully unleash the GitLab powers, introduce yourself to [Continuous Integration automation][3] 

1. create the `.gitlab-ci.yml` file in your project, see [Quick start guide][quickstart]. The runners are already configured.  
   There's a list of CI templates available for most common languages.
1. If you need to build your Dockerfile and create a container from it, adjust this `.gitlab-ci.yml` template:

   ```yaml
   image: docker:git
   
   before_script:
     - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN gitlab-registry.nautilus.optiputer.net
   
   stages:
     - build-and-push
   
   build-and-push-job:
     stage: build-and-push
     tags:
       - build-as-docker
     script:
       - docker build -t gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA:0:8} .
       - docker tag gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:${CI_COMMIT_SHA:0:8} gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}:latest
       - docker push gitlab-registry.nautilus.optiputer.net/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}
   ``` 

1. Go to `CI / CD -> Jobs` tab to see in amazement your job running and image being uploaded to your registry. 
1. From the Registry tab get the URL of your image to be included in your pod definition:

   ```yaml
   spec:
     containers:
     - name: my-container
       image: gitlab-registry.nautilus.optiputer.net/<your_group>/<your_project>:<optional_tag>
   ``` 

[1]: https://about.gitlab.com/what-is-gitlab/
[2]: https://about.gitlab.com/product/source-code-management/ 
[3]: https://about.gitlab.com/product/continuous-integration/ 
[4]: https://gitlab.nautilus.optiputer.net
[5]: https://docs.gitlab.com/ee/gitlab-basics/
[dockerhub]: https://www.docker.com 
[project]: https://docs.gitlab.com/ee/gitlab-basics/create-project.html 
[registry]: https://docs.gitlab.com/ee/user/project/container_registry.html
[quickstart]: https://docs.gitlab.com/ce/ci/quick_start/

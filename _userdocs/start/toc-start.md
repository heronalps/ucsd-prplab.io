---
layout: collection-userdocs
title: Where to start
date: 2019-01-10
short: userdocs
categories: user
order: 0
---

This section contains pages that show how to start using Nautilus cluster.

Use menu on the left for topics in this section.<br>
Use tabs in the top navigation bar to switch to another section.


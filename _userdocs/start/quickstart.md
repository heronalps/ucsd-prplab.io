---
layout: collection-userdocs
title: Quick Start 
date: 2019-01-10
short: userdocs
categories: user
order: 2
---

**Containers are stateless. ALL your data WILL BE GONE FOREVER when container restarts, unless you store it in a persistent volume.**

**Container restart is a normal thing in k8s cluster. Expect it.**

1. [Install][1] the kubectl tool
1. Login to [PRP Nautilus portal][2]  and click the **Get Config** link 
on top right corner of the page to get your configuration file
1. Save the file as **config** and put the file in your <home>/.kube folder. This
folder may not exists on your machine, to create it execute:
  ```bash
  mkdir ~/.kube
  ```

1. Any cluster admin can promote you to admin, and any admin can promote you to user. Make sure you are promoted from **guest** to **user** (and have a namespace
assigned to you) or **admin** by getting a confirmation from either admin or cluster admin.

1. If you've become an admin, you can start creating your own namespaces at this time by going to the **Manage namespaces** section in the portal. If you've become a user, make sure your namespace's admin added you to some namespace. One way to verify is to go to **Namespaces** link under the
**Services** menu (while logged on the portal). If you are assigned to any
namespaces they will be listed in the output.
 
1. Test kubectl can connect to the cluster using a command line tool: 
```bash
kubectl get pods -n your_namespace
```
It's possible there are no pods in your namespace yet. If you've got `No resources found.`, this indicates your namespace is empty and you can start running in it.

1. To learn more about kubernetes use the following links for the official documentation and
   tutorials.
   - [kubernetes basics][3]
   - [tutorials][5]
   - [kubectl tool][4]
   - [kubectl cheatsheet][6]
   
    Please note that not all examples will work in our cluster because of security policies. You are limited to see what's happening in your own namespace, and nobody else can see your running pods.

1. MANDATORY read the [Policies](/userdocs/start/policies/) page

1. [Proceed](/userdocs/running/toc-running/) to creating your first ML job in kubernetes

[![Guided kubernetes meditation](/images/med.jpeg "Guided kubernetes meditation")](https://medium.com/@gabe_50302/a-guided-kubernetes-meditation-63cc4193582c)

[1]: https://kubernetes.io/docs/tasks/tools/install-kubectl/
[2]: https://nautilus.optiputer.net
[3]: https://kubernetes.io/docs/tutorials/kubernetes-basics/
[4]: https://kubernetes.io/docs/reference/kubectl
[5]: https://kubernetes.io/docs/tutorials/
[6]: https://kubernetes.io/docs/reference/kubectl/cheatsheet/


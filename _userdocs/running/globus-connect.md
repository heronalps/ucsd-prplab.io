---
layout: collection-userdocs
title: Globus-connect
date: 2019-03-25
short: userdocs
categories: user
order: 70
---

We have a [globus-connect][1] Gitlab repository that provides step by step
instructions and all needed files for running globus-connect container and
creating a personal globus endpoint.

[1]: https://gitlab.nautilus.optiputer.net/prp/globus-connect

---
layout: collection-userdocs
title: Regions
date: 2019-04-08
short: userdocs
categories: user
order: 90
---

Our cluster combines various hardware resources from multiple universities and other organizations. By default you can only run in nodes having **env** label equals to **production** 
See the output of 
```bash
kubectl get nodes -L env
```

If your project is related to one of these persons:

|--|--|--|--|--|
| Ken Kreutz-Delgado| Tajana Simunic Rosing| Amit K. Roy Chowdhury| Walid Najjar| 
| Nikil Dutt| Trevor Darrell| Lise Getoor| Anshul Kundaje| 
| Gary Cottrell| Frank Wuerthwein| Hao Su| Dinesh Bharadia| 
| YangQuan Chen| Jeff Krichmar| Charless Fowlkes| Padhraic Smyth| 
| James Demmel| Yisong Yue| Shawfeng Dong| Rajesh Gupta| 
| Todd Hylton| Falko Kuester| Jurgen Schulze| Arun Kumar| 
| Ron Dror| Ravi Ramamoorthi| John Sheppard| Nuno Vasconcelos| 
| Ramakrishna Akella| Manmohan Chandraker| Baris Aksanli| Dimitris Achlioptas| 
| Ilkay Altintas| Brad Smith| Christopher Paolini| Jerry Sheehan| 


you can request your namespace to be assigned to nodes labelled as Chase-CI
("chaseci"). **This will give you more GPU nodes and shorter wait time.**

In addition, there are GPU nodes attached to our cluster from two video rooms:

| room  | nodes | GPU type| used as general pool |
|--|--|--|--|
| UCSD Suncave | 34 | 2 x 1080 or 2 x 1080Ti| no |
| UCMerced Wave| 10 | 2 x 1080| yes|

Because these nodes can be used for demos at anytime, any job running on those can be cancelled without any warning. 
If you're fine with this, you can ask for access to those - it's a great way to quickly run a large-scale job on multiple nodes with 
zero wait time. 

##### Special regions

We also have several regions which are remote enough to have slower access to
our ceph storage or have special hardware, and by default jobs are not being scheduled on them. But you can experiment with those and take advantage of being away from the crouds.

To use those special nodes, add toleration for "region" in your pod spec:

```
tolerations:
- key: "region"
  operator: "Equal"
  value: "allow"
  effect: "NoSchedule"
```

This will let your pod to be scheduled on those, but to request this, you'll have to use nodeSelector:

```
nodeSelector:
  kubernetes.io/hostname: <node_name>
```

The nodes are:

1. `fiona.its.hawaii.edu` - 128G RAM, 32 cores, 14*220G=3T of SSD NFS storage

1. `k8s-ravi-01.calit2.optiputer.net` - 256G RAM, 32 cores, 2.2T SSD NFS, 8 TITAN Xp (has some issues with CUDA, most GPU apps don't work on it. You can try if yours work)

1. Korea: `dtn2-daejeon.kreonet.net` - 128G RAM, 12 cores, 21T HDD NFS and `dtn-gpu2.kreonet.net` - 128G RAM, 48 cores, 6 TITAN Xp

Access to NFS storage is provided on request to namespace.
---
layout: collection-userdocs
title: Services
date: 2019-01-25
short: userdocs
categories: user
order: 15
---

For permanently running pods that provide some services you can use the [deployment controller](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/). It will ensure the requested number of pod replicas are running in the cluster, and start new ones if old for some reason stop.

Minimal example of a deployment to keep 1 instance of nginx server running:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    k8s-app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      k8s-app: nginx
  template:
    metadata:
      labels:
        k8s-app: nginx
    spec:
      containers:
      - image: nginx
        name: nginx-pod
        resources:
          limits:
            cpu: 1
            memory: 4Gi
          requests:
            cpu: 100m
            memory: 500Mi
```
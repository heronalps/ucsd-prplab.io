---
layout: collection-userdocs
title: GPU jobs
date: 2019-01-10
short: userdocs
categories: user
order: 30
---

#### Running GPU pods

Use the [tensorflow example pod][1] definition to create your own pod and deploy it to kubernetes.
You can try running this example in your namespace with:
```bash
kubectl create -n your_namespace -f https://gitlab.com/ucsd-prp/prp_k8s_config/raw/master/tensorflow-example.yaml
```
and destroy with
```bash
kubectl delete -n your_namespace -f https://gitlab.com/ucsd-prp/prp_k8s_config/raw/master/tensorflow-example.yaml
```
This example requests 1 GPU device. You can have up to 8 per node. If you request GPU devices in your pod, 
kubernetes will auto schedule your pod to the appropriate node. There's no need to specify the location manually. To request NVIDIA Tesla K40c GPUs 
use [this example][2]

**You should always delete your pod** when your computation is done to let other users use the GPUs.
Consider using [Jobs][3] whenever possible to ensure your pod is not wasting GPU time.
If you have never used Kubernetes before, see this [Tensorflow And Jupyter: Step By Step tutorial][4].

#### Choosing GPU type 

We have a variety of GPU flavors attached to Nautilus. This table describes the types of GPUs available for use.

**NOTE**: Not all nodes are available to all users. You can consult about your available resources in [rocketchat][5]. 
Labs connecting their hardware to our cluster have preferential access to all our resources.

Node | GPU Type | Count
---|---|---
evldtn.evl.uic.edu | M4000 | 1
fiona8-0.calit2.uci.edu | 1080 | 8
fiona8-1.calit2.uci.edu | 1080 | 8
fiona8-2.calit2.uci.edu | 1080 | 8
fiona8.ucsc.edu | 1080Ti | 8
hydra.gi.ucsc.edu | 1080Ti | 2
k8s-bafna-01.calit2.optiputer.net | 1080Ti | 8
k8s-bharadia-01.sdsc.optiputer.net | 1080Ti | 8
k8s-bharadia-02.sdsc.optiputer.net | 1080Ti | 8
k8s-bharadia-03.sdsc.optiputer.net | 1080Ti | 8
k8s-bharadia-04.sdsc.optiputer.net | 1080Ti | 8
k8s-chase-ci-01.calit2.optiputer.net | 1080Ti | 8
k8s-chase-ci-01.noc.ucsb.edu | 1080Ti | 8
k8s-chase-ci-02.calit2.optiputer.net | 1080Ti | 8
k8s-chase-ci-03.calit2.optiputer.net | 1080Ti | 8
k8s-chase-ci-04.calit2.optiputer.net | 1080Ti | 8
k8s-chase-ci-05.calit2.optiputer.net | 1080Ti | 8
k8s-chase-ci-06.calit2.optiputer.net | 1080Ti | 8
k8s-chase-ci-07.calit2.optiputer.net | 2080Ti | 8
k8s-chase-ci-08.calit2.optiputer.net | 2080Ti | 8
k8s-chase-ci-08.calit2.optiputer.net | 2080Ti | 8
k8s-gpu-01.calit2.optiputer.net | 1080 | 8
k8s-gpu-02.calit2.optiputer.net | titan-x | 8
k8s-gpu-03.sdsc.optiputer.net | 1080Ti | 8
k8s-gpu-1.ucr.edu | 1080Ti | 8
k8s-gpu-1.ucsc.edu | 1080Ti | 8
k8s-gpu-2.ucsc.edu | 1080Ti | 8
k8s-ravi-01.calit2.optiputer.net | titan-x | 8
k8s-tyan-gpu-01.sdsu.edu | K40 | 4
knuron.calit2.optiputer.net | K40 | 2
lyra-01.evl.uic.edu | 1070 | 1
patternlab.calit2.optiputer.net | M40 | 2
prp-gpu-1.t2.ucsd.edu | 1080Ti | 8
prp-gpu-2.t2.ucsd.edu | 1080Ti | 8
prp-gpu-3.t2.ucsd.edu | 1080Ti | 8
wave-head.ucmerced.edu | 1080 | 2
wave[00-09].ucmerced.edu | 1080 | 2

To use a specific type of GPU add the affinity definition to you pod yaml
file. The example below specifies *1080Ti* GPU:
```yaml
 affinity:
   nodeAffinity:
     requiredDuringSchedulingIgnoredDuringExecution:
       nodeSelectorTerms:
       - matchExpressions:
         - key: gpu-type
           operator: In
           values:
           - 1080Ti
```

[1]: https://gitlab.com/ucsd-prp/prp_k8s_config/raw/master/tensorflow-example.yaml
[2]: https://gitlab.com/ucsd-prp/prp_k8s_config/raw/master/tensorflow-k40.yaml
[3]: /userdocs/running/jobs/
[4]: /userdocs/running/jupyter/
[5]: https://rocket.nautilus.optiputer.net

---
layout: collection-userdocs
title: Monitoring
date: 2019-01-25
short: userdocs
categories: user
order: 60
---

When you run your jobs, it's your responsibility to make sure they are running as intended, without overrequesting the resources. Our [Grafana](https://grafana.nautilus.optiputer.net/) page is a great resource to see what your jobs are doing.

To get an idea how much resources your jobs are using, go to [namespace dashboard](https://grafana.nautilus.optiputer.net/dashboard/db/kubernetes-compute-resources-namespace-pods) and choose your namespace. Your requests percentage for memory and CPU should be as close to 100% as possible. Also check the [GPU dashboard](https://grafana.nautilus.optiputer.net/dashboard/db/k8s-compute-resources-namespace-gpus) for your namespace to make sure the utilization is above 40%, and ideally is close to 100%.
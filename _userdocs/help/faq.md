---
layout: collection-userdocs
title: FAQ
date: 2019-01-23
short: userdocs
categories: user
group: help
order: 2
---

1. How do i access nautilus?  See [Get access][1]
1. How do I use Kubernetes? See [Quick Start][2]
1. How do I use S3? See [Storage][3]
1. Where is Nautilus Located?<br>
Nautilus is a heterogeneous, distributed cluster, with computational resources of various shapes and 
sizes made available by research institutions spanning multiple continents! Check out the Cluster Map 
to see where the nodes are located. 

[1]: /userdocs/start/get-access/
[2]: /userdocs/start/quickstart/
[3]: /userdocs/storage/ceph/#ceph_s3

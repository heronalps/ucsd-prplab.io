---
layout: collection-userdocs
title: EdgeFS
date: 2019-04-11
short: userdocs
categories: user
order: 2
---

We have experimental installs of [EdgeFS][1] in several locations providing NFS (shared), iSCSI (exclusive) and S3X (object store + posix) access. Please refer to the [Regions](/userdocs/running/regions/) section for information on using those.

[1]: http://edgefs.io/

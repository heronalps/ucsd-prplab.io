---
layout: collection-userdocs
title: Nextcloud
date: 2019-02-08
short: userdocs
categories: user
order: 3
---

We provide access to the [Nextcloud][1] [instance][2] running in our cluster and using our ceph storage.
It's similar to other file sharing systems ([Dropbox](https://www.dropbox.com), [Google Drive](https://www.google.com/drive/) etc) and can be used to get data in the cluster, temporary stage the results, share data and so on. If you're planning to use it for large datasets, please contact us first with the usage plan.

## Using nextcloud from shell

To access your NextCloud storage from shell (or Jupyter), you can use the [rclone](https://rclone.org/) tool. It's already installed in our [Jupyterlab](https://jupyterhub.nautilus.optiputer.net) service.

To get access, create new rclone config:

```
jovyan@jupyter-dmishin-40ucsd-2eedu:~$ rclone config
2019/04/23 17:05:08 NOTICE: Config file "/home/jovyan/.config/rclone/rclone.conf" not found - using defaults
No remotes found - make a new one
n) New remote
s) Set configuration password
q) Quit config
n/s/q> n
name> nextcloud
Type of storage to configure.
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
 1 / A stackable unification remote, which can appear to merge the contents of several remotes
   \ "union"
 2 / Alias for a existing remote
   \ "alias"
 3 / Amazon Drive
   \ "amazon cloud drive"
 4 / Amazon S3 Compliant Storage Provider (AWS, Alibaba, Ceph, Digital Ocean, Dreamhost, IBM COS, Minio, etc)
   \ "s3"
 5 / Backblaze B2
   \ "b2"
 6 / Box
   \ "box"
 7 / Cache a remote
   \ "cache"
 8 / Dropbox
   \ "dropbox"
 9 / Encrypt/Decrypt a remote
   \ "crypt"
10 / FTP Connection
   \ "ftp"
11 / Google Cloud Storage (this is not Google Drive)
   \ "google cloud storage"
12 / Google Drive
   \ "drive"
13 / Hubic
   \ "hubic"
14 / JottaCloud
   \ "jottacloud"
15 / Koofr
   \ "koofr"
16 / Local Disk
   \ "local"
17 / Mega
   \ "mega"
18 / Microsoft Azure Blob Storage
   \ "azureblob"
19 / Microsoft OneDrive
   \ "onedrive"
20 / OpenDrive
   \ "opendrive"
21 / Openstack Swift (Rackspace Cloud Files, Memset Memstore, OVH)
   \ "swift"
22 / Pcloud
   \ "pcloud"
23 / QingCloud Object Storage
   \ "qingstor"
24 / SSH/SFTP Connection
   \ "sftp"
25 / Webdav
   \ "webdav"
26 / Yandex Disk
   \ "yandex"
27 / http Connection
   \ "http"
Storage> 25
** See help for webdav backend at: https://rclone.org/webdav/ **

URL of http host to connect to
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
 1 / Connect to example.com
   \ "https://example.com"
url> https://nextcloud.nautilus.optiputer.net/remote.php/webdav/
Name of the Webdav site/service/software you are using
Enter a string value. Press Enter for the default ("").
Choose a number from below, or type in your own value
 1 / Nextcloud
   \ "nextcloud"
 2 / Owncloud
   \ "owncloud"
 3 / Sharepoint
   \ "sharepoint"
 4 / Other site/service or software
   \ "other"
vendor> 1
User name
Enter a string value. Press Enter for the default ("").
user> <your nextcloud username>
Password.
y) Yes type in my own password
g) Generate random password
n) No leave this optional password blank
y/g/n> y
Enter the password:
password: <your password, or create a token in settings if using 2-factor>
Confirm the password:
password:
Bearer token instead of user/pass (eg a Macaroon)
Enter a string value. Press Enter for the default ("").
bearer_token>
Remote config
--------------------
[nextcloud]
type = webdav
url = https://nextcloud.nautilus.optiputer.net/remote.php/webdav/
vendor = nextcloud
user = dimm
pass = *** ENCRYPTED ***
--------------------
y) Yes this is OK
e) Edit this remote
d) Delete this remote
y/e/d> y
Current remotes:

Name                 Type
====                 ====
nextcloud            webdav

e) Edit existing remote
n) New remote
d) Delete remote
r) Rename remote
c) Copy remote
s) Set configuration password
q) Quit config
e/n/d/r/c/s/q> q
```

Then copy in your data:

```
rclone copy -P nextcloud:/Downloads .
```

[1]: https://nextcloud.com/
[2]: https://nextcloud.nautilus.optiputer.net

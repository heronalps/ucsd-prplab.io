# Instructions for making edits for PRP website 

These instructions summarize steps needed to add new posts or contents 
or edit existing ones.

- [Introduction](#introduction)
  - [Editing Basics](#editing-basics)
- [New files](#new-files)
  - [File headers](#file-headers)
  - [File contents ](#file-contents)

## Introduction 

The files in this repository include the layout and configuration of the whole
website plus the contents of individual pages and posts.  If your task is to
add a new content such as a new event or news item please follow the steps below.

### Editing Basics

1. DO NOT EDIT **_config.yml** at the top level.  This is a main jekyll layout file 
   for the whole site. Changing it may adversely affect  all website layout.
   This file is edited only  by the website maintainers. 

1. The following folders contain website layout logic and DO NOT need edits
   unless you are the website maintainer and are changing its style or layout :

   * _includes/
   * _layouts/ 
   * _sass/ 
   * assets/ 
   * css/

1. Most of the edits will be adding new or updating existing files in
   **_posts/**, **_news/** or **_events/**. 

1. Please use spaces and not TABS in any files. Using TABS will break the layout for the
   website **search** function.

1. Characters to use in file names are alphanumeric and a dash :

   * a-z A-Z
   * 0-9

   no spaces in files names.

1. New files naming convention: 

   * **date-file-name** for files in _posts/, for example : 

     1. 2018-09-17-bozeman-workshop-2018.md will be rendered at the website as http://NAME/bozeman-workshop-2018/
     1. 2018-10-10-EVENT.html will be rendered at the website as http://NAME/EVENT/
 
 	 where NAME is a website address 

   * **file-name** for files in _news/ and  _events/, for example : 

     1. news-1.md 
     1. news-about-prp.md 
     1. news-2019.html 
     1. event-2018-aug.html 
     1. workshop-2018-bozeman.html

The sections below outline what needs to be edited.  The edits can be done from the GitLab  gui via web browser 

## New files 

Each post or page is either markdown syntax or html syntax file. 
Jekyll requires a special header to be placed at the beginning of each file.
The file contents then follow the header and are styled according to the file
format (markdown or html)


### File headers

A header is a formatted list of key-value pairs. 
The following list describes used variables and their meaning:

- **---** 3 dashes  on the first line start the header, required 
- **layout:**   a layout template. Usually "post", or "page" 
- title: Your Title Here
- **date:** a date for the post item. Formats:

  * Nov 05, 2015
  * 2018-08-06
- **categories:**  starts a list of categories, each item on the list is on a
  new line beginning with a dash. Only for some headers
	  
  * "cat1" - first item in the list 
  * "cat2" - second item in the list 
- **tags: []**, optional, reserved for later use 
- **imagesrc: myimage1.jpg** file name for the image to use with the page/post
  Image formats jpg, png, jpeg.
- **location:** used to show the date and location, for files in _events/ 
-  **workshop_info:** for workshop dates and location
-  **workshop_url:**  website URL prefix for a workshop website
   It must start with "/" and the following string corresponds to the file name without extension.
- **---** 3 dashes  on the line end the header, required 
- an empty  line following 3 dashes.
   
Examples of headers for different types of postings: 

1. **Header for files in _posts/**

   ```
   ---
   layout: post
   title: Your Title Here
   author: First Last
   date: Nov 5, 2017
   ---
   ```

1. **Header for files in _news/**

   The header for files in _news/ will need to use "categories:" variable. 
   First item in the list is always "news", second  is the year of posting. 
   
   ```
   ---
   layout: post
   title: Your Title Here
   date: 2018-09-13
   categories: 
   - "news" 
   - "2018"
   tags: []
   imagesrc: pic.jpg
   ---
   ```
   
1. **Header for files in _events/**
   
   There 2 types of headers for the  posts in _events/

   * files describing workshops events
   
     ```
     ---
     title: FIONA Workshop
     layout: page
     workshop_date: August 2 - 5, 2018, Bozeman, Montana
     workshop_url: /bozeman-workshop-2018
     date: 2018-09-15
     categories:
     - "workshop"
     - "2018"
     ---
     ```
   
   * files describing all other events
   
     ```
     ---
     layout: post
     title: Event October X
     date: 2018-09-17
     dates: October 3-5, 2018
     location: San Diego, CA
     categories: 
     - "event"
     - "2018"
     tags: []
     imagesrc: event1.jpg
     ---
     ```
   
### File contents

The contents of a post or a page are placed after the header
The files can be written in markdown format or html format. 

- **markdown files ** - have ".md" extension and use markdown syntax.
- **html files ** - have ".html" extension and use html syntax


# Instructions for Maintainers 

This is a summary of instructions how to change layout of the website

- [Prerequisites](#prerequisites)
- [Running Jekyll locally](#running-jekyll-locally)
- [Site configuration](#site-configuration)
- [Site contents](#site-contents)
- [Editing](#editing)
  - [Adding a workshop website](#adding-a-workshop-website)

## Prerequisites
-----------------

* Install [jekyll][Jekyll] on your personal laptop where you will be doing
  development
* Check out website repository 

  ```bash
  git clone https://gitlab.com/ucsd-prp/ucsd-prp.gitlab.io
  ```

## Running Jekyll locally

To serve  the website pages on your local host,
open your favorite terminal window and from
the top level of the checked-out repository execute:
  ```bash
  ./serve.sh
  ```
This will start the jekyll daemon and open a browser window with the website
at the address "http://localhost:4000". If you run another server on port
4000  on your host you can change the port in serve.sh 

A new directory "_site" will be created by the jekyll daemon nd this is
where all create files for a website are placed. 

When any changes are done to any layout or post/page files in the repo, jekyll
recreates updates files in "_site/" 

When "_config.yml" is changed the jekyll process will need to be restarted.

## Site Configuration

At the top level of the checked-out repository there are following
configuration files

1. Files needed for jekyll, inherited from the original theme: Gemfile, Gemfile.lock 
   feed.xml, sitemap.xml

1. **_config.yml** - this  is a main jekyll layout file 
   for the whole website.

1. _includes/ contains files that are used to build a specific part of each
   post or page. For example:

   - head.html - creates all meta, link, css references at the beginning of the
	 page
   - nav.html - creates a navigation bar  at the top of the page
   - foot.html - creates a footer at the bottom of each page
   - copyright.html - created copyright portion below the footer 

1. _layouts/  contains files that specify  different layouts. 

   - landing.html - used only for the main site entry page (index.html at the top of the repo)
   - default.html - the base level layout that builds structure for header,
	 main part and footer of each page. 
   - post.html - inherits from default.html, used for posts
   - page.html - inherits from default.html, used for pages
   - collection-*.html  - inherits from default.html used for layout 
     of specific pages or posts for news, meeting, etc.

   The inheritance can be specified with the header using base file name :

   ```
   ---
   layout: default
   ---
   ```

1.  css/ contains a main css file and a few theme-specific css files that are
    used for styling
    - main.scss - main css file that references  other css 
	- noscript.css - imported by head.html
    - font-awesome.min.css - imported by main.css

1. _sass/ contains all remaining css files that are needed for styling. 
   To add a new css file (only needed for a specific portion of the website,
   for example a specific workshop or event):

   * add a file in _sass/_FILENAME.sccs 
   * add its name to the @import list in css/main.sccs file

1. assets/ contains theme specific fonts, icons, and java script.
   A new javascript file can be added to assets/js/

1.  search.html  and search_data.json provide search function for the files on
the website

## Site Contents

1. Site main entry point is written in index.html file.

1. The rest of the files  at the top of the  repo are entry points for
   each tab  in the navigation bar of the main page.

   If there will be multiple pages for a specific category, for example
   "news", the layout will have a file and a directory :
   
   * news.md  - entry point
   * _news/ - all news related posts and pages

   Similarly, for "events", there is events.md and _events/

   If there will be a single page  that is referenced form the navigation
   bar, a single file is created, for example, partners.md

1. The rest of pages and posts files will be placed according to their
   category in the following directories and their subdirectories: 
   
   * _posts/
   * _news/
   * _events/

1. All images are in images/ and corresponding subdirectories.  For example, 
   images for any post in "news" should be placed in images/news/


## Editing

* Do not use TABS in any files, use spaces. TABS will break the layout for the
  whole website **search** function.


### Adding a workshop website

These are the files that need to be edited with the correct content.
For example to add a workshop WORKSHOPNAME website:

1. create a directory _posts/WORKSHOPNAME
   All pages with content will be placed in this subdirectory
   The naming convention similar to already existing templates 
1. create subdirectory for images specific to this workshop as **images/WORKSHOPNAME/**
   and put all needed images there


[Jekyll]: http://jekyllrb.com/

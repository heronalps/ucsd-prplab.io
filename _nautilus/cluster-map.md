---
layout: default
title: Cluster Map
short: cluster map
siteimage: nautilus.png
custom_css: cluster_map_styles
---

<a href="#byinst" class="subtitle">Information by institution </a> | 
<a href="#bynode" class="subtitle">Information by node </a>  

<div class="dfn" id="byinst">Information grouped by institution</div>

<div class="map-and-legend">
  <div class="map-container">
    {% google_map src="_data/cluster_map" groupby="institution" %}
  </div>
    {% include cluster_map/inst-legend.md %}
</div>
{% include cluster_map/inst-table.md %}

<a href="{{ page.url }}"> Back to top </a> 

<div class="dfn" id="bynode">Information grouped by node</div>
<div class="map-and-legend" id="bynode">
  <div class="map-container">
    {% google_map src="_data/cluster_map" groupby="node" %}
  </div>
  {% include cluster_map/node-legend.md %}
</div>
{% include cluster_map/node-table.md %}

<a href="{{ page.url }}" > Back to top </a> 

#### Notes
(1)  The marker locations are approximate.<br>
(2) The coordinates are from [GeoIP API](https://ipstack.com)<br>
(3) If city/region data is unavailable, USA markers default to somewhere in rural Kansas.
   See [this article][lawsuit] about a law suit filed by the unfortunate farmer living there.
   There is one node (osg.kans.nrp.internet2.edu) which is actually located in
   Kansas.<br>
(4) A ~0.1 mile jitter is added to each marker to avoid 100% overlap if two markers have the same coordinates.

#### Source Code
The python code used to create data for this page can be found in the [GitLab repo][cluster-map-repo]

<a href="{{ page.url }}" > Back to top </a> 


[cluster-map-repo]: https://gitlab.nautilus.optiputer.net/nautilus-wiki/cluster-map 
[lawsuit]: https://www.theguardian.com/technology/2016/aug/09/maxmind-mapping-lawsuit-kansas-farm-ip-address

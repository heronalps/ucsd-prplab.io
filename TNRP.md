---
layout: page
title: Toward a National Research Platform
short: tnrp
siteimage: nrp.png
---

The PRP/TNRP Pilot Engagement Team Meetings every other Monday  at 10 PT/ 11 MT/ 12 CT/ 1 ET.
<br>Email distribution list: `prp-nrppilot-engagement@internet2.edu`.
<br>Please send an <a href="mailto:cchaplin@internet2.edu" class="sbutton">email</a> containing the new director's
name/email/title and they will be added to the calendar invite and the distribution list for that group.

**Connection detail**
: **ZOOM:** Join from PC, Mac, Linux, iOS or Android <a href="https://internet2.zoom.us/j/7203992979]" class="sbutton">https://internet2.zoom.us/j/7203992979</a>
: **Or iPhone one-tap**
:   US: +16699006833,,7203992979#  or +16465588656,,7203992979#
: **Or Telephone:**
:   Dial (for higher quality, dial a number based on your current location):
:   US: +1 669 900 6833 or +1 646 558 8656
:	Meeting ID: 720 399 2979
:	International numbers available: <a href="https://zoom.us/u/bEUSqfiar" class="sbutton">https://zoom.us/u/bEUSqfiar</a>
: **Or an H.323/SIP room system** H.323:

  |--|--|--|
  | 162.255.37.11 (US West)| 162.255.36.11 (US East)| 221.122.88.195 (China)|
  | 115.114.131.7 (India)| 213.19.144.110 (EMEA)| 202.177.207.158 (Australia)|
  | 209.9.211.110 (Hong Kong)| 64.211.144.160 (Brazil)| 69.174.57.160 (Canada)|
:  Meeting ID: 720 399 2979
: **SIP: 7203992979@zoomcrc.com**
: **Or Skype for Business (Lync)** <a hrf="https://internet2.zoom.us/skype/7203992979" class="sbutton">https://internet2.zoom.us/skype/7203992979</a>

<div class="border">
PRP and TNRP Meeting Agendas
</div>
[2019 PRP/TNRP Pilot Running Notes][notes]<br>
[2019, January 31 - 10:00AM Pacific - Agenda and attachements][2019-01-31]

[wral]: https://www.wraltechwire.com/2017/08/17/what-does-it-take-to-build-a-national-big-data-superhighway/
[notes]: https://docs.google.com/document/d/1JfQO_odoUyYaNfCzRWz_cvrN2gtQj9ovbq9GJUkZyG0/edit
[2019-01-31]: http://lsmarr.calit2.net/archive/PRP_Call__2019_01_31_Agenda.pdf

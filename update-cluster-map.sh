#!/bin/bash

# Install kubectl to conda container
apt-get update && apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y --allow-unauthenticated kubectl

# Generate cluster map
git clone https://gitlab.nautilus.optiputer.net/nautilus-wiki/cluster-map.git
cd cluster-map
./get_ipstack_api_key.sh
pip install -r requirements.txt
python update_map.py
cd ..

# Add results to repository
rsync -avz cluster-map/out/* .

# Configure git
git config --global user.email "nautilusmapbot@gmail.com"
git config --global user.name "Nautilus Map Bot"

# Commit and push changes (if any)
git add _data/ images/
git commit -m "Update cluster-map"
git push http://nautilus-map-bot:${MAP_BOT_PAT}@gitlab.com/${CI_PROJECT_PATH}.git HEAD:${PUSH_BRANCH}


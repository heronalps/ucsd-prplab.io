---
layout: collection-admindocs
title: Kubernetes
date: 2019-01-10
short: admindocs
categories: admin
order: 0
---

This section contains pages that provide information how to setup Kubernetes.

Use menu on the left for topics in this section.<br>
Use tabs in the top navigation bar to switch to another section.

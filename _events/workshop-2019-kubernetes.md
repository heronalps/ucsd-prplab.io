---
layout: post
title: K8s and Grafana 
date: 2019-03-17
dates: 2019 March 17
location: Calit2, CA
eventurl: /lajolla2019k8s/
ehost: California Institute for Telecommunications and Information Technology - UC San Diego
imagesrc: event-k8s.png
categories: 
- "workshop"
- "2019"
tags: []
---

In this workshop, the PRP team will show participants beginner and advanced features and in-depth examples of using:
(1) Kubernetes, an open-source orchestrator for containerized applications;
(2) Grafana, open platform for creating analytical and monitoring dashboards;
(3) Prometheus, monitor for Kubernetes services
[Registration][registration]

[registration]: https://nscc.typeform.com/to/D1Nbtw

---
layout: page
title: Big Data and The Earth Sciences
date: 2017-06-01
dates: 2017 May 31 - June 02
location: UCSD, La Jolla, CA
siteimage: events.png
categories:
- "workshop"
- "2018"
---

### Grand Challenges Workshop 
May 31 -June 2, 2017 Atkinson Hall, UC San Diego Welcome Scott L. Sellars, University of California, San Diego

<iframe width="560" height="315" src="https://www.youtube.com/embed/da386rc8CkI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The goal of the The Big Data and Earth Sciences: Grand Challenges Workshop is to bring thought leaders in Big Data and Earth Sciences together for 
a three day, intensive workshop to discuss what is needed to advance our understanding and predictability of the Earth systems and to highlight key 
technological advances and methods that are readily available (or will be soon) to assist this advancement. With the ever growing quantity and 
quality of hyper-dimensional earth science data (satellite and ground based observations and cutting edge Numerical Weather Prediction (NWP) models), 
the advancements in machine learning (e.g. supervised, unsupervised and semi-supervised learning techniques), and the progress made in the application 
of Graphical Processing Units (GPUs) and GPU clusters, we now have an unprecedented opportunity and challenge to engage these computational advances to 
improve our understanding of the complex nature of the interactions between various earth science events, their variables and their impacts on society (flooding, drought, agriculture, etc.).

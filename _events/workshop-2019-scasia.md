---
layout: post
title: Move That Data! 
date: 2019-03-12
dates: 2019 March 12
location: Singapore
registration: https://nscc.typeform.com/to/D1Nbtw
siteimage: events.png
categories:
- "workshop"
- "2019"
---

####  @ Supercomputing Asia 2019 Conference

<img class="imgright" src="/images/events/move.png">
The movement of data on the internet is a major challenge for researchers in BIG Sciences. Over the past decade, we can see that it is 
not only in BIG Science that generates large amount of data, but also Internet of things (IoT) as standards of the data resolution being captured continue to evolve. 

We have also seen increases of network bandwidth, from mega to Giga, and 100s of Gbps.  Despite this high bandwidth capacity, researchers are 
still not experiencing the corresponding increase in the speed of data transfer.  This is due to multiple factors: server, transport protocol, etc.

**Move That Data!** was the first-ever Data Mover challenge on a global scale and the inaugural edition held in Singapore at [Asia 2019 Supercomputing Conference][scasia]. 
The challenge aimed to bring together experts from industry and academia, in a bid to test their software across servers located in 
various countries (USA, Singapore, Australia, Japan) that are connected by 100G international networks.  

[scasia]:  https://www.nscc.sg/

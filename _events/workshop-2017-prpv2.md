---
layout: page
title: PRPv2
date: 2017-02-21
dates: 2017 Feb 21
location: UCSD, La Jolla, CA
siteimage: events.png
categories:
- "workshop"
- "2018"
---

<iframe width="560" height="315" src="https://www.youtube.com/embed/zQ-_V7KU7-s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

For the last three years, the National Science Foundation (NSF) has made a series of competitive grants to over 100 U.S. universities to 
aggressively upgrade their campus network capacity for greatly enhanced science data access. NSF is now building on that distributed investment by 
funding a $5 million, five-year award to UC San Diego and UC Berkeley to establish a Pacific Research Platform (PRP), a science-driven high-capacity 
data-centric **freeway system** on a large regional scale. Within a few years, the PRP will give participating universities and other research 
institutions the ability to move data 1,000 times faster compared to speeds on today's inter-campus shared Internet. 

The goal of this Workshop is to debate and discuss the goals of PRPv2. The technical design and build-out of the PRP project is being 
conducted in two phases  with the aim of achieving the following goals: 

| Year | Phase | Goals |
| -- | -- | -- |
| 1-2 | PRPv1 | 1 Create a scalable network design for optimizing data transfer |
| 3-5 | PRPv2 | 2 Evolve to IPv6 with Cooperating Research Groups<br> 3 Create OpenFlow as a firewall and explore other trust and security features|

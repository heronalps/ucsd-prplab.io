---
layout: page
title: 3NRP Workshop 2019                   
date: 2019-3-20
dates: September 24-25, 2019
location: La Jolla, CA
siteimage: events.png
imagesrc: 3nrp_date.png
categories: 
- "event"
- "2019"
tags: []
---

#### The Third National Research Platform Workshop

More information to be announced soon.


---
layout: post
title: Superhighway for Data-Intensive Science by Saemmool Lee
date: 2018-10-08
categories: 
- "news" 
tags: [3D modeling]
imagesrc: CAVE_UCMerced.jpg
---

The Center for Information Technology Research in the Interest of Society (CITRIS) and the Banatao Institute highlight some of the exciting projects being enabled by the PRP, including the HearstCAVE, which is part of the At-Risk Cultural Heritage and the Digital Humanities project funded by one of four University of California President's Research Catalyst Awards.
[Read the full article][1].

[1]: https://citris-uc.org/superhighway-for-data-intensive-science/ 


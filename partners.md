---
layout: post
title: Institutional Partners
short: Partners
siteimage: partners.png
---
Thanks to a broad network of partners, the PRP is realizing
high-performance, data-focused network interoperability and enabling broader
access to data to a wide range of researchers at institutions across the
country and the globe.  The Pacific Research Platform is creating a regional,
end-to-end, science-driven "big data freeway" with the help of the following
critical institutions:

- [AARnet][aarnet]
- [Chameleon Cloud][chameleon]
- [CITRIS and the Banatao Institute][citris]
- [Clemson University][clemson]
- [Corporation for Network Education Initiatives in California (CENIC)][cenic]
- [Korea Institute of Science and Technology Information (KISTI)/Korea Research Environment Open NETwork (KREONET)][kisti kreonet]
- [Metropolitan Research and Education Network (MREN)][mren]
- [Montana State University][montana u]
- [National Center for Atmospheric Research (NCAR)][ncar]
- [National Center for Supercomputing Applications (NCSA)][ncsa]
- [Northwestern University ][northwestern u]
- [StarLight Software Defined Networking Exchange (StarLight SDX)][starlight sdx]
- [University of Hawaii System][u hawaii]
- [University of Amsterdam][u amsterdam]
- [University of Illinois at Chicago][u illinois]
- [University of Tokyo][u tokyo]
- [Energy Sciences Network][esnet]

[aarnet]: https://www.aarnet.edu.au
[citris]: https://citris-uc.org/
[chameleon]: https://www.chameleoncloud.org/
[clemson]: http://www.clemson.edu/
[cenic]: http://cenic.org/
[kisti kreonet]: http://www.kisti.re.kr/eng/
[mren]: http://www.mren.org/
[montana u]: http://www.montana.edu/
[ncar]: http://ncar.ucar.edu/
[ncsa]: http://www.ncsa.illinois.edu/
[northwestern u]: http://www.northwestern.edu/
[starlight sdx]: http://www.startap.net/starlight/
[u hawaii]:  https://www.hawaii.edu/
[u amsterdam]: https://www.uva.nl/en/home
[u illinois]: https://www.uic.edu/
[u tokyo]: http://www.u-tokyo.ac.jp/en/index.html
[esnet]: http://www.es.net/
